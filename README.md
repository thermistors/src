![Sacramento State University - Sacramento](https://www.csus.edu/brand/_internal/_images/logo-imgs/primary_horizontal_3_color_tag_wht_hnd.jpg)

# **CpE 185 - Computer Interfacing**
## Lab 5: Automate an existing process

An STM32 microprocessor with a thermister collects ambiant room temperature, converts the signal to digital and communicates the data through USB using UART.<br/>
A Raspberry Pi is used to recieve and log the UART data. The Raspberry Pi is also used to host a flask server where weather data is aggregated around a provided zip code using the [weather.gov API](https://www.weather.gov/documentation/services-web-api).<br/><br/> 
Using the collected data from weather.gov, we will graph and analyze the data from the reported temperature and the ambiant room temperature on the hosted flask webpage.<br/>
Depending on the severity of the room temperature, a series of LED�s connected to a PI will illuminate to represent the current state of the room temperature (comfortable, uncomfortable, extreme).<br/>

<br/><br/>**Block Diagram:**<br/>
![Block Diagram](https://bitbucket.org/thermistors/src/raw/8928b8c682ca7c06631b002b97da798ceb0daf5f/static/block_diagram.png)

<br/><br/>**Finite State Machine (FSM):**<br/>
![FSM](https://bitbucket.org/thermistors/src/raw/8928b8c682ca7c06631b002b97da798ceb0daf5f/static/fsm.png)

<br/><br/>**Wiring:**<br/>
![Wiring](https://bitbucket.org/thermistors/src/raw/8928b8c682ca7c06631b002b97da798ceb0daf5f/static/wiring.png)

<br/><br/>**Web Server:**<br/>
![Web Server](https://bitbucket.org/thermistors/src/raw/8928b8c682ca7c06631b002b97da798ceb0daf5f/static/enter_zip_code.png)

<br/><br/>**Thermister Graph:**<br/>
![Thermister Graph](https://bitbucket.org/thermistors/src/raw/8928b8c682ca7c06631b002b97da798ceb0daf5f/static/thermister_graph.png)

